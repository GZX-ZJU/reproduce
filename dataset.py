import os
import re
import json
import zipfile
import urllib.request
from itertools import chain

import kaggle
import pandas as pd
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split

from settings import *


"""
module to download data from network
"""


def tang_poetry(path='tang_poetry.txt',
                max_vocab=10000,
                max_length=40,
                start_token='[',
                stop_token=']'):
    """

    :param path: the data file path relative to DATA_PATH
    :param max_vocab: max vocabulary size
    :param max_length: max sentence length
    :param start_token: token to indicate the start of a sentence
    :param stop_token: token to indicate the end of a sentence
    :return: train_x, train_y, word_index
    """
    path = os.path.join(DATA_PATH, path)
    if os.path.exists(path):
        with open(path, 'r', encoding='utf-8') as f:
            j = json.load(f)
    else:
        url = ('http://raw.githubusercontent.com/dream-catcher/learning_blogs/'
               'master/Quantangshi_Corpus/clean_quantangshi_emerson_20180127.json')
        urllib.request.urlretrieve(url, path)
        with open(path, 'r', encoding='utf-8') as f:
            j = json.load(f)

    def get_one(x):
        s = x[4]
        s = s.strip()
        return s.split('\n')

    poetry = list(map(get_one, j))
    poetry = list(chain(*poetry))

    tokenizer = Tokenizer(num_words=max_vocab, char_level=True)
    tokenizer.fit_on_texts(poetry)
    tokenizer.word_index[start_token] = len(tokenizer.word_index) + 1  # add start and stop token to word index
    tokenizer.word_index[stop_token] = len(tokenizer.word_index) + 1

    poetry = list(filter(lambda x: len(x) < max_length-1, poetry))
    p_x = list(map(lambda x: start_token + x, poetry))
    p_y = list(map(lambda x: x + stop_token, poetry))
    train_x = tokenizer.texts_to_sequences(p_x)
    train_y = tokenizer.texts_to_sequences(p_y)
    train_x = pad_sequences(train_x, max_length)
    train_y = pad_sequences(train_y, max_length)

    return train_x, train_y, tokenizer.word_index


def boson_ner(path='boson_ner.zip',
              max_vocab=10000,
              max_length=300,
              tag_format='BMESO'):
    path = os.path.join(DATA_PATH, path)
    if os.path.exists(path):
        pass
    else:
        url = 'http://bosonnlp.com/resources/BosonNLP_NER_6C.zip'
        urllib.request.urlretrieve(url, path)

    with zipfile.ZipFile(path) as file:
        content = file.read('BosonNLP_NER_6C/BosonNLP_NER_6C.txt').decode('utf-8')

    entity_types = {'time', 'location', 'person_name', 'org_name', 'company_name', 'product_name'}

    content = content.replace('\\n', '')
    sentences = content.split('\n')
    targets = []
    sequences = []
    for s in sentences:
        sentence, target = _process_sentence(s, tag_format)
        assert len(sentence) == len(target)
        targets.append(target)
        sequences.append(sentence)

    tokenizer = Tokenizer(num_words=max_vocab, char_level=True)
    tokenizer.fit_on_texts(sequences)

    tokenizer_tag = Tokenizer(num_words=len(tag_format)*len(entity_types)+1)
    tokenizer_tag.fit_on_texts(targets)

    x = tokenizer.texts_to_sequences(sentences)
    x = pad_sequences(x, max_length)
    y = tokenizer_tag.texts_to_sequences(targets)
    y = pad_sequences(y, max_length)

    return x, y, tokenizer, tokenizer_tag.word_index


def _process_sentence(sentence, tag_format):

    split = re.split(r'({{.*?:.*?}})', sentence)
    target = []
    sentence = []
    for i, e in enumerate(split):
        if i % 2 == 1:
            word, tag = _process_word(e, tag_format)
            target += tag
            sentence.append(word)
        else:
            target += ['O' for _ in range(len(e))]
            sentence.append(e)

    return ''.join(sentence), target


def _process_word(entity, tag_format):
    """ process one entity

    :param str entity: example {{entity_type:word}}
    :param str tag_format: 'BMESO' or 'BIO'
    :return target

    >>> _process_word('{{person_name:施某}}')
    ['B-person_name', 'E-person_name']

    """
    entity = entity.strip('{}')
    split = entity.split(':', maxsplit=1)
    tag, word = split
    target = []
    if tag_format == 'BMESO':
        if len(word) == 1:
            target.append('S-{}'.format(tag))
        else:
            target.append('B-{}'.format(tag))
            for _ in range(len(word)-2):
                target.append('M-{}'.format(tag))
            target.append('E-{}'.format(tag))
    else:
        target.append('B-{}'.format(tag))
        for _ in range(len(word)-1):
            target.append('I-{}'.format(tag))

    return word, target


# todo: add THUCNews dataset support (download and preprocessing)


if __name__ == '__main__':
    # tang_poetry()
    boson_ner()
