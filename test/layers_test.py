import unittest
import numpy as np
from keras.layers import Input, Embedding
from keras.models import Model
from layers.transformer import SelfAt, Encoder


class TransformerTest(unittest.TestCase):
    def test_self_at(self):
        inputs = Input(shape=(self.seq_len, self.embed_size))
        outputs = SelfAt(self.attention_depth, self.attention_depth, self.num_heads)(inputs)
        output_shape = outputs.shape.as_list()
        self.assertTrue(output_shape[-1] == self.attention_depth)

    def test_encoder(self):
        inputs = Input(shape=(self.seq_len, ))
        x = Embedding(self.vocab_size, self.embed_size, input_length=self.seq_len)(inputs)
        out = Encoder(self.attention_depth,
                      self.attention_depth,
                      self.num_heads,
                      self.output_units)(x)
        out_shape = out.shape.as_list()
        self.assertTrue(out_shape[-1] == self.output_units)

        model = Model(inputs=inputs, outputs=out)
        model.compile('adam', loss='categorical_crossentropy', metrics=['acc'])

        random_input = np.random.randint(0, 4, [2, self.seq_len], dtype=np.int32)
        model.predict(random_input)

    def setUp(self):
        self.seq_len = 10
        self.embed_size = 25
        self.attention_depth = 20
        self.num_heads = 2
        self.output_units = 20
        self.vocab_size = 5


if __name__ == '__main__':
    unittest.main()
