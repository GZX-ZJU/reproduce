import os
import unittest

from models.text.classification import *
from models.text.generation import *
from dataset import tang_poetry

from settings import *


class TextClassificationTest(unittest.TestCase):

    def test_fast_text(self):
        model = FastText(self.seq_len, self.vocab_size, self.embed_size)
        model.summary()

    def test_text_cnn(self):
        model = TextCNN(self.seq_len,
                        self.vocab_size,
                        self.embed_size,
                        self.hidden_size,
                        self.kernel_size)
        model.summary()

    def test_text_rnn(self):
        model = TextRNN(self.seq_len,
                        self.vocab_size,
                        self.embed_size,
                        self.hidden_size)
        model.summary()

    def test_text_rcnn(self):
        model = RCNN(self.seq_len,
                     self.vocab_size,
                     self.embed_size,
                     self.hidden_size,
                     self.kernel_size)
        model.summary()

    def test_text_transformer(self):
        model = Transformer(self.seq_len,
                            self.vocab_size,
                            self.embed_size,
                            2,
                            self.key_depth,
                            self.value_depth,
                            self.num_heads)
        model.summary()

    def test_text_paragraph(self):
        model = ParagraphClassifier(self.n_seq,
                                    self.seq_len,
                                    self.vocab_size,
                                    self.embed_size,
                                    self.hidden_size)
        model.summary()

    def setUp(self):
        self.n_seq = 40
        self.seq_len = 10
        self.vocab_size = 100
        self.embed_size = 15
        self.hidden_size = 20
        self.kernel_size = 3
        self.key_depth = 384
        self.value_depth = 384
        self.num_heads = 6


class TextGenerationTest(unittest.TestCase):
    def test_base_lstm(self):
        train_x, train_y, word_index = tang_poetry(max_length=self.max_len)
        vocab_size = len(word_index)
        lstm_gen = LSTMGen(self.max_len,
                           vocab_size,
                           self.embed_size,
                           self.rnn_units,
                           word_index)
        lstm_gen.fit(train_x, train_y, epochs=1)
        sentences = lstm_gen.generate_sentence(2)
        lstm_gen.save(os.path.join(SAVED_MODEL_PATH, 'text_gen/lstm_gen.h5'))
        print(sentences)

    def setUp(self):
        self.embed_size = 64
        self.max_len = 40
        self.rnn_units = 100


if __name__ == "__main__":
    unittest.main()
