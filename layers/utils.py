import keras.backend as K


__all__ = [
    'shape_list'
]


def shape_list(x):
    static = K.int_shape(x)
    shape = K.shape(x)

    ret = []
    for i in range(len(static)):
        dim = static[i]
        if dim is None:
            dim = shape[i]
        ret.append(dim)
    return ret
