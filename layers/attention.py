from keras.layers import Layer
import keras.backend as K


class AttentionWeightedAverage(Layer):
    def __init__(self,
                 **kwargs):
        super().__init__(**kwargs)

    def build(self, input_shape):
        assert len(input_shape) == 3
        self.W = self.add_weight(name='W',
                                 shape=[input_shape[2], 1],
                                 initializer='glorot_uniform')

    def call(self, inputs, mask=None):
        logits = K.dot(inputs, self.W)
        x_shape = K.shape(inputs)
        logits = K.reshape(logits, [x_shape[0], x_shape[1]])
        ai = K.exp(logits - K.max(logits, axis=-1, keepdims=True))

        if mask is not None:
            mask = K.cast(mask, K.floatx())
            ai = ai * mask

        at_weights = ai / (K.sum(ai, axis=1, keepdims=True) + K.epsilon())
        weighted_inputs = inputs * K.expand_dims(at_weights)
        result = K.sum(weighted_inputs, axis=1)
        return result

    def compute_mask(self, inputs, mask=None):
        return None

    def compute_output_shape(self, input_shape):
        return input_shape[0], input_shape[2]
