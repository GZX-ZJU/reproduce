from keras.layers import Layer, Dense
from keras.models import Model
import keras.backend as K
from .utils import *


__all__ = [
    'SelfAt',
    'Encoder'
]


class SelfAt(Layer):
    def __init__(self,
                 key_depth,
                 value_depth,
                 num_heads,
                 **kwargs):
        if key_depth % num_heads != 0:
            raise ValueError("Key depth {} must be divisible by the number of"
                             "attention heads {}".format(key_depth, num_heads))

        if value_depth % num_heads != 0:
            raise ValueError("Value depth {} must be divisible by the number of"
                             "attention heads {}".format(value_depth, num_heads))

        self._key_depth = key_depth
        self._value_depth = value_depth
        self._num_heads = num_heads

        super().__init__(**kwargs)

    def build(self, input_shape):
        input_dim = input_shape[-1]
        self.query_kernel = self.add_weight(name='query_kernel',
                                            shape=(input_dim, self._key_depth),
                                            initializer='uniform',
                                            trainable=True)
        self.key_kernel = self.add_weight(name='key_kernel',
                                          shape=(input_dim, self._key_depth),
                                          initializer='uniform',
                                          trainable=True)
        self.value_kernel = self.add_weight(name='value_kernel',
                                            shape=(input_dim, self._value_depth),
                                            initializer='uniform',
                                            trainable=True)
        super().build(input_shape)

    def call(self, inputs, mask=None, **kwargs):
        q = K.dot(inputs, self.query_kernel)
        k = K.dot(inputs, self.key_kernel)
        v = K.dot(inputs, self.value_kernel)

        q = split_heads(q, self._num_heads)  # [batch_size, num_head, seq_len, depth_per_head]
        k = split_heads(k, self._num_heads)
        v = split_heads(v, self._num_heads)

        depth_per_head = self._key_depth / self._num_heads
        q *= depth_per_head ** -0.5  # divided by sqrt(depth_per_head)
        logits = K.batch_dot(q, k, axes=3)
        if mask is not None:
            mask = K.cast(mask, K.floatx())
            logits += (mask - 1) * 1e5
        weights = K.softmax(logits)
        # v = tf.matmul(weights, v)
        v = K.batch_dot(weights, v, axes=[3, 2])
        v = combine_heads(v)  # [batch_size, seq_len, depth]
        return v

    def compute_output_shape(self, input_shape):
        # shape of input tensor if [batch_size, seq_len, units]
        assert len(input_shape) == 3
        return input_shape[0], input_shape[1], self._value_depth

    def compute_mask(self, inputs, mask=None):
        return mask


class Encoder(Model):
    def __init__(self,
                 key_depth,
                 value_depth,
                 num_heads,
                 output_units,
                 activation=None,
                 residual_conn=False):
        super().__init__()

        self.self_at = SelfAt(key_depth, value_depth, num_heads)
        self.dense = Dense(output_units, activation=activation)
        self.residual_conn = residual_conn
        self.output_units = output_units

    def call(self, inputs, **kwargs):
        if self.residual_conn:
            x_shape = shape_list(inputs)
            assert x_shape[-1] == self.output_units
        x = self.self_at(inputs)
        x = self.dense(x)
        if self.residual_conn:
            x = x + inputs
        return x

    def compute_output_shape(self, input_shape):
        # shape of input tensor if [batch_size, seq_len, units]
        assert len(input_shape) == 3
        return input_shape[0], input_shape[1], self.output_units

    def compute_mask(self, inputs, mask):
        return mask


def split_heads(x, num_heads):
    shape = shape_list(x)
    m = shape[-1]
    x_tmp = K.reshape(x, shape[:-1] + [num_heads, m // num_heads])
    return K.permute_dimensions(x_tmp, [0, 2, 1, 3])  # [batch_size, num_heads, seq_length, depth_per_head]


def combine_heads(x):
    x_tmp = K.permute_dimensions(x, [0, 2, 1, 3])  # [B, L, N, H]
    shape = shape_list(x_tmp)
    x_tmp = K.reshape(x_tmp, shape[:-2] + [shape[-2] * shape[-1]])
    return x_tmp
