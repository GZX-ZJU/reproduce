import os

import numpy as np
from keras.datasets import fashion_mnist, cifar10
from keras.preprocessing.image import ImageDataGenerator

from .classification import SimpleCNN, LargerCNN
from settings import *


def simple_cnn():
    (x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()
    input_shape = (28, 28, 1)
    model = SimpleCNN(input_shape)
    model.compile('adam', loss='sparse_categorical_crossentropy', metrics=['acc'])

    # simple image pre processing
    x_train = x_train / 255
    x_test = x_test / 255
    x_train = np.expand_dims(x_train, -1)
    x_test = np.expand_dims(x_test, -1)

    model.fit(x_train, y_train, batch_size=64, epochs=2)
    model.evaluate(x_test, y_test)
    model.save(os.path.join(SAVED_MODEL_PATH, 'img_classify/simple_cnn.h5'))


def larger_cnn():
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    datagen = ImageDataGenerator(featurewise_center=True,
                                 featurewise_std_normalization=True,
                                 rotation_range=20,
                                 width_shift_range=0.2,
                                 height_shift_range=0.2,
                                 horizontal_flip=True)
    datagen.fit(x_train)
    input_shape = (32, 32, 3)
    model = LargerCNN(input_shape)
    model.compile('adam', loss='sparse_categorical_crossentropy', metrics=['acc'])

    epochs = 10
    batch_size = 256
    model.fit_generator(datagen.flow(x_train, y_train, batch_size=batch_size),
                        steps_per_epoch=len(x_train) // batch_size, epochs=epochs)

    model.evaluate_generator(datagen.flow(x_test, y_test, batch_size=1000), steps=10)
    model.save(os.path.join(SAVED_MODEL_PATH, 'img_classify/larger_cnn.h5'))
