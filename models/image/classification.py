from keras.layers import Input, Conv2D, Dense, MaxPool2D, Flatten
from keras.models import Model


def SimpleCNN(input_shape, data_format='channels_last'):
    inputs = Input(shape=input_shape)
    x = Conv2D(32, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(inputs)  # 28 * 28 * 32
    x = Conv2D(32, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 28 * 28 * 32
    x = MaxPool2D()(x)  # 14 * 14 * 32
    x = Conv2D(64, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 14 * 14 * 64
    x = Conv2D(64, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 14 * 14 * 64
    x = MaxPool2D()(x)  # 7 * 7 * 64
    x = Conv2D(128, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)
    x = Conv2D(128, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 7 * 7 * 128
    x = Flatten()(x)
    x = Dense(512, activation='relu')(x)
    x = Dense(512, activation='relu')(x)
    y = Dense(10, activation='softmax')(x)
    model = Model(inputs=inputs, outputs=y)
    model.summary()
    return model


def LargerCNN(input_shape, data_format='channels_last'):
    inputs = Input(shape=input_shape)
    x = Conv2D(32, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(inputs)  # 32 * 32 * 32
    x = Conv2D(32, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 32 * 32 * 32
    x = Conv2D(32, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 32 * 32 * 32
    x = Conv2D(32, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 32 * 32 * 32
    x = MaxPool2D()(x)  # 14 * 14 * 32
    x = Conv2D(64, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 16 * 16 * 64
    x = Conv2D(64, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 16 * 16 * 64
    x = Conv2D(64, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 16 * 16 * 64
    x = MaxPool2D()(x)  # 8 * 8 * 64
    x = Conv2D(128, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 8 * 8 * 128
    x = Conv2D(128, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 8 * 8 * 128
    x = Conv2D(128, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 8 * 8 * 128
    x = MaxPool2D()(x)  # 4 * 4 * 128
    x = Conv2D(256, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 4 * 4 * 256
    x = Conv2D(256, 3,
               padding='same',
               activation='relu',
               data_format=data_format)(x)  # 4 * 4 * 256
    x = Flatten()(x)
    x = Dense(512, activation='relu')(x)
    x = Dense(256, activation='relu')(x)
    y = Dense(10, activation='softmax')(x)
    model = Model(inputs=inputs, outputs=y)
    model.summary()
    return model
