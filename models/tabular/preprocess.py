import numpy as np


class TabularPreprocessor(object):
    def __init__(self,
                 norm_form='standard',
                 missing_value=None,
                 fill_strategy='mean',
                 fill_value=0,
                 cat_encode='one-hot',
                 missing_as_feature=True,
                 feature_types=None):
        """Preprocess tabular data, including the following steps:

        1. fill missing value
        2. scale continuous data to standard normal distribution or to range [0, 1]
        3. convert categorical feature to one-hot encoding or intensity encoding

        :param norm_form: standard, minmax or None, default is standard.
            Scale each feature using standard scaler or minmax scaler.
        :param missing_value: None or int value, default is None.
            The value means missing, Usually nan, sometimes -1.
        :param fill_strategy: mean, median, most_frequent or constant.
            replace missing value with mean, median, most_frequent or the constant value.
        :param fill_value: Only useful when fill_strategy is constant, default is 0.
            When norm_form is standard, the filling_missing is always going to be zero.
        :param cat_encode: one-hot or continuous, default one-hot.
            Using one-hot or continuous value (0, 1, 2, ...) to encode categorical feature.
        :param missing_as_feature: boolean, default is True.
            Only useful when cat_encode is one-hot. If True, [1, 0, ...] denotes missing, else [0, 0, ...]
        :param list feature_types: feature type of each column.
            if None, feature column type is inferred from data
        """
        self._norm_form = norm_form
        self._missing_value = missing_value
        self._fill_value = fill_value
        self._fill_strategy = fill_strategy
        self._cat_encode = cat_encode
        self._missing_as_feature = missing_as_feature
        self._feature_types = feature_types

        self._fitted = False
        self._statistics = []

    def fit_transform(self, x):
        self.fit(x)
        return self.transform(x)

    def fit(self, x):
        """

        :param np.ndarray x: example features
        """
        if self._feature_types is None:
            self._feature_types = self.infer_type(x)

        self._fitted = True

    def transform(self, x):
        if not self._fitted:
            raise ValueError('You need call the fit method to fit on data')
        pass

    def infer_type(self, x):
        """

        :param np.ndarray x: 2-D feature matrix
        :return list: list of feature type of each column
        """
        assert x.ndim == 2, "X should be 2-D matrix"
        feature_types = []
        num_features = x.shape[1]

        for i in range(num_features):
            feature_types.append(self.infer_type(x[:, i]))
        return feature_types

    def infer_one_column_type(self, x):
        """

        :param x: one feature column of sample
        :return: 'categorical' or 'numerical'
        """
        assert x.ndim == 1
        missing_index = x != self._missing_value
        actual_data = x[missing_index]

        return 'categorical' or 'numerical'
