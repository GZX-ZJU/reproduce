import os

from keras.datasets import imdb
from keras.layers import *
from keras.models import Model
from keras.optimizers import Adam, RMSprop
from keras.preprocessing.sequence import pad_sequences

from layers.transformer import *
from dataset import tang_poetry
from .generation import LSTMGen
from settings import *


"""
对于各种的模型的快速尝试

"""


def base_lstm():
    max_len = 400
    vocab_size = 10000
    embed_size = 128

    (train_x, train_y), (test_x, test_y) = imdb.load_data('imdb.npz',
                                                          num_words=vocab_size,
                                                          skip_top=0,
                                                          maxlen=max_len,
                                                          seed=113,
                                                          start_char=1,
                                                          oov_char=2,
                                                          index_from=3)
    train_x = pad_sequences(train_x, maxlen=max_len)
    test_x = pad_sequences(test_x, maxlen=max_len)

    inputs = Input(shape=(max_len, ))
    x = Embedding(vocab_size, embed_size, input_length=max_len,
                  embeddings_initializer='glorot_uniform')(inputs)
    x = SpatialDropout1D(0.3)(x)
    x = Bidirectional(LSTM(128,
                           return_sequences=True,
                           kernel_initializer='glorot_uniform'))(x)
    x = Lambda(lambda i: K.mean(i, axis=-1))(x)
    x = Dropout(0.3)(x)
    y = Dense(1, activation='sigmoid',
              kernel_initializer='glorot_uniform')(x)

    model = Model(inputs=inputs, outputs=y)
    model.summary()
    model.compile(Adam(0.1), loss='binary_crossentropy', metrics=['acc'])

    model.fit(train_x, train_y, batch_size=128, epochs=4, validation_data=(test_x, test_y))


def attention_base():
    max_len = 400
    vocab_size = 10000
    embed_size = 128
    key_depth = 256
    val_depth = 256
    num_heads = 4
    output_units = 64

    (train_x, train_y), (test_x, test_y) = imdb.load_data('imdb.npz',
                                                          num_words=vocab_size,
                                                          skip_top=0,
                                                          maxlen=max_len,
                                                          seed=113,
                                                          start_char=1,
                                                          oov_char=2,
                                                          index_from=3)
    train_x = pad_sequences(train_x, maxlen=max_len)
    test_x = pad_sequences(test_x, maxlen=max_len)

    inputs = Input(shape=(max_len, ))
    x = Embedding(vocab_size, embed_size,
                  input_length=max_len,
                  embeddings_initializer='glorot_uniform')(inputs)
    # x = at_encoder(x, key_depth, val_depth, num_heads, output_units)
    x = SpatialDropout1D(0.3)(x)
    x = Encoder(key_depth, val_depth, num_heads, output_units)(x)
    # x = Lambda(lambda i: K.mean(i, 1))(x)
    x = GlobalAveragePooling1D()(x)
    x = Dropout(0.3)(x)
    y = Dense(1, activation='sigmoid')(x)

    model = Model(inputs=inputs, outputs=y)
    model.summary()
    model.compile(Adam(0.01), loss='binary_crossentropy', metrics=['acc'])

    model.fit(train_x, train_y, batch_size=128, epochs=3, validation_data=(test_x, test_y))


def base_lstm_gen():
    max_len = 40
    embed_size = 64
    rnn_units = 100
    train_x, train_y, word_index = tang_poetry(max_length=max_len)
    vocab_size = len(word_index)
    lstm_gen = LSTMGen(max_len, vocab_size, embed_size, rnn_units, word_index)

    for i in range(5):
        lstm_gen.fit(train_x, train_y, epochs=1)
        print('After {} epoch training'.format(i+1))
        print('generate result: {}'.format(lstm_gen.generate_sentence(1)))

    lstm_gen.save(os.path.join(SAVED_MODEL_PATH, 'text_gen/lstm_gen3.h5'))

