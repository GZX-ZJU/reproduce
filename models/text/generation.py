import numpy as np

from keras.layers import Input, Embedding, LSTM, Bidirectional, Dense
from keras.models import Model, load_model
from keras.optimizers import Adam
from keras.losses import sparse_categorical_crossentropy
from keras.preprocessing.sequence import pad_sequences
import keras.backend as K


__all__ = [
    'LSTMGen'
]


class LSTMGen(object):
    def __init__(self,
                 max_len: int,
                 vocab_size: int,
                 embed_size: int,
                 rnn_units: int,
                 word_index: dict,
                 index_word: dict=None,
                 start_token='[',
                 stop_token=']'
                 ):
        """

        :param max_len: supported max_len to generate sentence
        :param vocab_size: vocabulary size, equal to len(word_index)
        :param embed_size: embedding size of word
        :param word_index: word to index (range from 1 to vocab_size), ...
        :param index_word: index to word. reverse dict of word_index
        """

        # do some checking
        if start_token not in word_index:
            raise ValueError('Make sure the start token is in the word index')
        if stop_token not in word_index:
            raise ValueError('Make sure the stop token is in the word index')
        assert vocab_size == len(word_index)

        self.max_len = max_len
        self.vocab_size = vocab_size
        self.embed_size = embed_size
        self.rnn_units = rnn_units
        self.word_index = word_index
        self.start_token = '['
        self.stop_token = ']'
        if index_word is None:
            self.index_word = dict(zip(word_index.values(), word_index.keys()))
        else:
            self.index_word = index_word
        self.model = self.build_model()

    def build_model(self):
        inputs = Input(shape=(self.max_len, ))
        embed = Embedding(self.vocab_size + 1, self.embed_size, mask_zero=True, input_length=self.max_len)(inputs)
        x = LSTM(self.rnn_units, return_sequences=True)(embed)
        y = Dense(self.vocab_size + 1, activation='softmax')(x)
        model = Model(inputs=inputs, outputs=y)
        model.compile(Adam(0.001), loss=sparse_categorical_crossentropy)
        model.summary()
        return model

    def fit(self, train_x, train_y, batch_size=128, epochs=1):
        if train_y.ndim == 2:
            train_y = np.expand_dims(train_y, -1)
        self.model.fit(train_x, train_y, batch_size, epochs)

    def generate_sentence(self, n=1, temperature=1.0):
        sentences = ['[' for _ in range(n)]
        changed = True
        while changed:
            s_id = self.preprocess_sentence(sentences)
            preds = self.model.predict(s_id, batch_size=n)  # [B, S, V] B: batch size, S: seq length, V: vocab size
            preds = preds[:, -1, :]  # [B, V]
            sentences, changed = self.sample_word(sentences, preds, temperature)

        result = []
        for s in sentences:
            if s[-1] == ']':
                result.append(s[1:-1])
            else:
                result.append(s[1:])
        return result

    def sample_word(self, sentences, preds, temperature=1.0):
        """

        :param sentences: 已经生成的未完成的句子
        :param preds: LSTM 输出的最后一个词的概率分布
        :param temperature: temperature 调节LSTM 输出的概率分布。temperature越大，pred 分布越均匀，句子多样性越好；
            temperature越小，pred分布越集中，句子多样性差
        :return: sentences, changed tag
        """
        if np.max(list(map(len, sentences))) > self.max_len:
            return sentences, False

        assert preds.ndim == 2
        preds = np.log(preds + K.epsilon()) / temperature
        preds = np.exp(preds)
        preds = preds / np.sum(preds, -1, keepdims=True)
        index = []
        for pred in preds:
            probas = np.random.multinomial(1, pred, 1)
            index.append(np.argmax(probas))

        result = []
        changed = False
        for i, s in enumerate(sentences):
            if s[-1] == self.stop_token:
                result.append(s)
            else:
                result.append(s + self.index_word.get(index[i], ''))
                changed = True

        return result, changed

    def preprocess_sentence(self, sentences):
        s_id = [self.word2id(s) for s in sentences]
        return pad_sequences(s_id, self.max_len)

    def word2id(self, sentence):
        return [self.word_index.get(w, 0) for w in sentence]

    def save(self, path, weights=True):
        if weights:
            self.model.save_weights(path)
        else:
            self.model.save(path)

    def load(self, path, weights=True):
        if weights:
            self.model.load_weights(path)
        else:
            self.model = load_model(path, compile=True)


# todo: 类似textgenrnn中的模型 textgenrnn为别人所写的一个包


# todo: 基于transformer 的文本生成模型
