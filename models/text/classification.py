from keras.layers import *
from keras.models import Model
from layers.auxiliary import Conv1DMask

from layers.transformer import Encoder
from layers.attention import AttentionWeightedAverage


__all__ = [
    'FastText',
    'TextCNN',
    'TextRNN',
    'RCNN',
    'Transformer',
    'ParagraphClassifier'
]


def FastText(seq_len,
             vocab_size,
             embed_size,
             classes=2,
             embedding_matrix=None):
    inputs = Input(shape=(seq_len, ))
    embed_layer = Embedding(vocab_size, embed_size, mask_zero=True, input_length=seq_len)
    if embedding_matrix is not None:
        embed_layer.set_weights([embedding_matrix])
    x = embed_layer(inputs)
    x = GlobalAveragePooling1D()(x)
    classes = 1 if classes <= 2 else classes
    act_fn = 'sigmoid' if classes <= 2 else 'softmax'
    outputs = Dense(classes, activation=act_fn)(x)
    model = Model(inputs=inputs, outputs=outputs)
    return model


def TextCNN(seq_len,
            vocab_size,
            embed_size,
            hidden_size,
            kernel_size,
            classes=2,
            pool_type='average',
            embedding_matrix=None):
    inputs = Input(shape=(seq_len, ))
    embed_layer = Embedding(vocab_size, embed_size, mask_zero=True, input_length=seq_len)
    if embedding_matrix is not None:
        embed_layer.set_weights([embedding_matrix])
    x = embed_layer(inputs)
    x = Conv1DMask(hidden_size, kernel_size, padding='same')(x)
    if pool_type == 'average':
        x = GlobalAveragePooling1D()(x)
    elif pool_type == 'max':
        x = GlobalMaxPool1D()(x)
    classes = 1 if classes <= 2 else classes
    act_fn = 'sigmoid' if classes <= 2 else 'softmax'
    outputs = Dense(classes, activation=act_fn)(x)
    model = Model(inputs=inputs, outputs=outputs)
    return model


def TextRNN(seq_len,
            vocab_size,
            embed_size,
            hidden_size,
            classes=2,
            rnn_type='lstm',
            bidirectional=False,
            embedding_matrix=None):
    inputs = Input(shape=(seq_len, ))
    embed_layer = Embedding(vocab_size, embed_size, mask_zero=True, input_length=seq_len)
    if embedding_matrix is not None:
        embed_layer.set_weights([embedding_matrix])
    x = embed_layer(inputs)
    rnn_cell = _get_cell(rnn_type, hidden_size)
    if bidirectional:
        x = Bidirectional(rnn_cell)(x)
    else:
        x = rnn_cell(x)
    x = GlobalAveragePooling1D()(x)
    classes = 1 if classes <= 2 else classes
    act_fn = 'sigmoid' if classes <= 2 else 'softmax'
    outputs = Dense(classes, activation=act_fn)(x)
    model = Model(inputs=inputs, outputs=outputs)
    return model


def RCNN(seq_len,
         vocab_size,
         embed_size,
         hidden_size,
         kernel_size,
         classes=2,
         rnn_type='lstm',
         bidirectional=False,
         embedding_matrix=None):
    inputs = Input(shape=(seq_len, ))
    embed_layer = Embedding(vocab_size, embed_size, mask_zero=True, input_length=seq_len)
    if embedding_matrix is not None:
        embed_layer.set_weights([embedding_matrix])
    x = embed_layer(inputs)
    x = Conv1DMask(hidden_size, kernel_size, padding='same')(x)
    rnn_cell = _get_cell(rnn_type, hidden_size)
    if bidirectional:
        x = Bidirectional(rnn_cell)(x)
    else:
        x = rnn_cell(x)
    x = GlobalAveragePooling1D()(x)
    classes = 1 if classes <= 2 else classes
    act_fn = 'sigmoid' if classes <= 2 else 'softmax'
    outputs = Dense(classes, activation=act_fn)(x)
    model = Model(inputs=inputs, outputs=outputs)
    return model


def Transformer(seq_len,
                vocab_size,
                embed_size,
                classes=2,
                key_depth=384,
                value_depth=384,
                num_heads=6,
                embedding_matrix=None):
    # todo: 将LSTM层替换为layers.transformer.Encoder
    # 其他层与TextRNN相同，构建模型
    inputs = Input(shape=(seq_len, ))
    embed_layer = Embedding(vocab_size, embed_size, mask_zero=True, input_length=seq_len)
    if embedding_matrix is not None:
        embed_layer.set_weights(embedding_matrix)
    x = embed_layer(inputs)
    x = Encoder(key_depth, value_depth, num_heads, output_units=value_depth // num_heads)(x)
    x = AttentionWeightedAverage()(x)
    classes = 1 if classes <= 2 else classes
    act_fn = 'sigmoid' if classes <= 2 else 'softmax'
    outputs = Dense(classes, activation=act_fn)(x)
    model = Model(inputs=inputs, outputs=outputs)
    return model


def ParagraphClassifier(n_seq,
                        seq_length,
                        vocab_szie,
                        embed_size,
                        rnn_units,
                        classes=2,
                        embedding_matrix=None):
    # todo: test mask support
    # todo: use THUCNews corpus to train and eval this model
    inputs = Input(shape=(seq_length, ))
    embed_layer = Embedding(vocab_szie, embed_size, mask_zero=True, input_length=seq_length)
    if embedding_matrix is not None:
        embed_layer.set_weights(embedding_matrix)
    x = embed_layer(inputs)
    x = Bidirectional(LSTM(rnn_units, return_sequences=True))(x)
    x = AttentionWeightedAverage()(x)
    model = Model(inputs=inputs, outputs=x)

    inputs = Input(shape=(n_seq, seq_length))
    x = TimeDistributed(model)(inputs)
    x = Bidirectional(LSTM(rnn_units, return_sequences=True))(x)
    x = AttentionWeightedAverage()(x)
    classes = 1 if classes <= 2 else classes
    act_fn = 'sigmoid' if classes <= 2 else 'softmax'
    outputs = Dense(classes, activation=act_fn)(x)
    model = Model(inputs=inputs, outputs=outputs)
    return model


def _get_cell(rnn_type, hidden_size):
    rnn_type = rnn_type.lower()
    if rnn_type == 'rnn':
        return RNN(hidden_size, return_sequences=True)
    elif rnn_type == 'lstm':
        return LSTM(hidden_size, return_sequences=True)
    elif rnn_type == 'gru':
        return GRU(hidden_size, return_sequences=True)
    else:
        return ValueError('unknown rnn cell type: {}'.format(rnn_type))
