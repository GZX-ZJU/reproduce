import keras.backend as K
import tensorflow as tf


c = tf.ConfigProto()
c.gpu_options.allow_growth = True
sess = tf.Session(config=c)
K.set_session(sess)
