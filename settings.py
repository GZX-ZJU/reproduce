import os


__all__ = [
    'ROOT_PATH',
    'DATA_PATH',
    'SAVED_MODEL_PATH'
]


ROOT_PATH = os.path.dirname(__file__)
DATA_PATH = os.path.join(ROOT_PATH, 'data')
SAVED_MODEL_PATH = os.path.join(ROOT_PATH, 'saved_model')
