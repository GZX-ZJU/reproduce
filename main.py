import models.text.fast_try as text_model
import models.image.fast_try as image_model


def main():
    # text_model.base_lstm()
    # text_model.attention_base()
    # text_model.base_lstm_gen()
    # image_model.simple_cnn()
    image_model.larger_cnn()


if __name__ == '__main__':
    main()
